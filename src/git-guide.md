class: center, middle

# Guía "rápida" de git

por `@kysxd`

---
class: middle

## ¿Git?

_Git_ es un software de control de versiones.

--
* Random three-letter combination that is pronounceable, and not actually used by any common UNIX command.

--
* Stupid. Contemptible and despicable. Simple.

--
* "Global Information Tracker": you're in a good mood, and it actually works for you. Angels sing, and a light suddenly fills the room.

--
* "Goddamn Idiotic Truckload of sh\*t": when it breaks.


--
```
$ man git

  git - the stupid content tracker
```

---
class: middle

# Conocimientos previos

--
* Branch, Checkout, Pull, Push, Merge, Rebase

--
* Commit, Tag

--
* Fork & Remote

---
class: middle

## ¿Por donde empiezo?

--
Sincronizando

--
* `$ git fetch origin`


--
Pero... lo importante es `main`

--
* `$ git checkout main`

--
* `$ git pull`

---
class: center, middle

## Ya estoy al día. ¿Ahora?

--
¡Comienza a echar codiguito!

---
class: middle

## Primeros pasos

--
* Crea un branch

--
* Haz cambios

--
* Guarda

--
* Merge/Pull request

--
* Toma un café

---
class: middle, center

## Espera, espera, espera...

--
Primero veamos algunas reglas

---
class: middle

## Branch

--
### Convención de nombres

Busca un acuerdo con el grupo de desarrollo


--
Por ejemplo: usuario/descripción

--
* `luisito/edicion-titulos`

* `kysxd/modificacion-perrona`

* `juanga/noa-noa`

---
class: middle

## Branch

* Ejemplo:

    `$ git branch kysxd/creacion-db-inventario`

    `$ git checkout kysxd/creacion-db-inventario`

--
* O en su versión reducida:

    `$ git checkout -b kysxd/creacion-db-inventario`

---
class: middle

### Lo común

Vease _git-flow_

* main
* develop
* feature/XXX
* release/XXX
* hotfix/XXX

### Lo que recomiendo

Vease _trunk based development_

* main
* feature/xxx

Source: https://trunkbaseddevelopment.com/

--
Super explicito: name/class/issue-number/description


--
* `lorena/bug-fix/i135/error-scope-lista-sucursal`

* `jose-juan/feature/i734/busqueda-inventario`

---
class: middle

## Commit

--
### Cambios granulares

Pequeños _commits_ lógicos;

--
de esa forma aprovechas las herramientas de `git`
(`blame`, `revert`)

---
class: middle

## Commit

Estás escribiendo una historia


--
No lo haga, compa:

  ```
  $ git commit -m "Borrado de tablas, cambio en la identación,
        borrado de archivo.txt, modicacion de titulo, reubicacion de archivos,
        eliminacion de código duplicado, correción de traducciones,..."
  ```


--
Es más util:

  ```
  $ git commit -m "Borrado de tablas"
  $ ...
  $ git commit -m "Cambio en la identación"
  $ ...
  $ git commit -m "Borrado de archivo.txt"
  $ ...
  ```

---
class: middle

### Recomendaciones

* Construye secuencialmente:

--
    * Prototipos

--
    * API-level functions

--
    * Consumidores de esas funciones

--
* Si un commit tiene más de 100 lineas
  re-evalua si hay una forma lógica de escribir la historia.

--
* Bug-fix no relacionados van en diferentes branches.

---
class: middle, center

## ¿Ya puedo hacer commit?

--
¡Go for it!


--
**\*Aplican restricciones**

---
class: middle

### Git add

--
Verifica qué es lo que se agrega

--
  > `$ git add -p`


--
Corrobora antes de agregar a la historia


--
  > `$ git status -v`

---
class: middle

### Git commit

--
Ahora sí, hora de grabar.


--
¿Lo subo a main?

--
**NO**

---
class: middle, center

### ¿Como agrego mis cambios a producción?

--
Pull/Merge request en su proveedor de hosting de confianza.


---
class: middle

### Más recomendaciones

--
* ¡Prueba tu código! (Vease TDD)

    _"Untested Code is Broken Code"_

--
* Continuous Integration

    _Returning the "soft" part to the "software"_

---
class: middle

### Entonces... ¿main = producción?

--
Dependiendo del proyecto (y su tiempo) es conveniente tener más de un branch

--
* `main`

    Donde vive todo _release_ (listo para producción)

* `develop(ment)`

    El branch más actualizado (no necesariamente estable)

---
class: middle

### Recuerda los eventos importantes

--
Usar tags ayuda a marcar los eventos importantes en la historia del proyecto...

--
```
  $ git tag v0.0.1
```

--
...o volver a ellos...

--
```
  $ git checkout v0.0.1
```

--
...o listarlos...

--
```
  $ git tag
```

---
class: middle, center

## Citas

TODO: Add quotation


(n.n)'

---
class: center, middle

Caiganle con las

# Preguntas

---
class: center, middle

TY


--
(Thank You)
