var slideshow = remark.create({
  sourceUrl: 'git-guide.md',
  countIncrementalSlides: false,
  highlightSpans: true,
  slideNumberFormat: '%current%',
  highlightLines: true,
});
